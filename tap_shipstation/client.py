"""REST client handling, including shipstationStream base class."""

from tracemalloc import start
from typing import Any, Dict, Iterable, List, Optional, Union

import requests
from memoization import cached
from singer_sdk.authenticators import BasicAuthenticator
from singer_sdk.helpers.jsonpath import extract_jsonpath
from singer_sdk.streams import RESTStream


class shipstationStream(RESTStream):
    """shipstation stream class."""

    url_base = "https://ssapi.shipstation.com"

    records_jsonpath = "$[*]"
    next_page_token_jsonpath = "$.[page,pages]"

    @property
    def authenticator(self) -> BasicAuthenticator:
        """Return a new authenticator object."""

        return BasicAuthenticator.create_for_stream(
            self,
            username=self.config.get("api_key"),
            password=self.config.get("api_password"),
        )

    @property
    def http_headers(self) -> dict:
        """Return the http headers needed."""
        headers = {}
        if "user_agent" in self.config:
            headers["User-Agent"] = self.config.get("user_agent")
        return headers

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""

        all_matches = extract_jsonpath(self.next_page_token_jsonpath, response.json())
        curren_page = next(iter(all_matches), None)
        total_pages = next(iter(all_matches), None)

        if curren_page == total_pages:
            return None

        return curren_page + 1

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""
        params: dict = {}
        params["pageSize"] = 500
        if next_page_token:
            params["page"] = next_page_token
        if self.replication_key:
            start_date = self.get_starting_replication_key_value(context)
            start_date = start_date.replace("T", " ").split(".")[0]
            params["sortBy"] = "ModifyDate"
            params["modifyDateStart"] = start_date
        return params
