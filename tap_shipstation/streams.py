from singer_sdk import typing as th

from tap_shipstation.client import shipstationStream


class OrdersStream(shipstationStream):
    """Define custom stream."""

    name = "orders"
    path = "/orders"
    records_jsonpath = "$.orders[*]"
    primary_keys = ["orderId"]
    replication_key = "modifyDate"

    schema = th.PropertiesList(
        th.Property("orderId", th.IntegerType),
        th.Property("orderNumber", th.StringType),
        th.Property("orderKey", th.StringType),
        th.Property("orderDate", th.DateTimeType),
        th.Property("createDate", th.DateTimeType),
        th.Property("modifyDate", th.DateTimeType),
        th.Property("paymentDate", th.DateTimeType),
        th.Property("shipByDate", th.DateTimeType),
        th.Property("orderStatus", th.StringType),
        th.Property("customerId", th.IntegerType),
        th.Property("customerUsername", th.StringType),
        th.Property("customerEmail", th.StringType),
        th.Property(
            "billTo",
            th.ObjectType(
                th.Property("name", th.StringType),
                th.Property("company", th.StringType),
                th.Property("street1", th.StringType),
                th.Property("street2", th.StringType),
                th.Property("street3", th.StringType),
                th.Property("city", th.StringType),
                th.Property("state", th.StringType),
                th.Property("postalCode", th.StringType),
                th.Property("country", th.StringType),
                th.Property("phone", th.StringType),
                th.Property("residential", th.StringType),
                th.Property("addressVerified", th.StringType),
            ),
        ),
        th.Property(
            "shipTo",
            th.ObjectType(
                th.Property("name", th.StringType),
                th.Property("company", th.StringType),
                th.Property("street1", th.StringType),
                th.Property("street2", th.StringType),
                th.Property("street3", th.StringType),
                th.Property("city", th.StringType),
                th.Property("state", th.StringType),
                th.Property("postalCode", th.StringType),
                th.Property("country", th.StringType),
                th.Property("phone", th.StringType),
                th.Property("residential", th.BooleanType),
                th.Property("addressVerified", th.StringType),
            ),
        ),
        th.Property(
            "items",
            th.ArrayType(
                th.ObjectType(
                    th.Property("orderItemId", th.IntegerType),
                    th.Property("lineItemKey", th.StringType),
                    th.Property("sku", th.StringType),
                    th.Property("name", th.StringType),
                    th.Property("imageUrl", th.StringType),
                    th.Property(
                        "weight",
                        th.ObjectType(
                            th.Property("value", th.NumberType),
                            th.Property("units", th.StringType),
                        ),
                    ),
                    th.Property("quantity", th.IntegerType),
                    th.Property("unitPrice", th.NumberType),
                    th.Property("taxAmount", th.NumberType),
                    th.Property("shippingAmount", th.NumberType),
                    th.Property("warehouseLocation", th.StringType),
                    # th.Property("options", th.ArrayType),
                    th.Property("productId", th.IntegerType),
                    th.Property("fulfillmentSku", th.StringType),
                    th.Property("adjustment", th.BooleanType),
                    th.Property("upc", th.StringType),
                    th.Property("createDate", th.DateTimeType),
                    th.Property("modifyDate", th.DateTimeType),
                )
            ),
        ),
        th.Property("orderTotal", th.NumberType),
        th.Property("amountPaid", th.NumberType),
        th.Property("taxAmount", th.NumberType),
        th.Property("shippingAmount", th.NumberType),
        th.Property("customerNotes", th.StringType),
        th.Property("internalNotes", th.StringType),
        th.Property("gift", th.BooleanType),
        th.Property("giftMessage", th.StringType),
        th.Property("paymentMethod", th.StringType),
        th.Property("requestedShippingService", th.StringType),
        th.Property("carrierCode", th.StringType),
        th.Property("serviceCode", th.StringType),
        th.Property("packageCode", th.StringType),
        th.Property("confirmation", th.StringType),
        th.Property("shipDate", th.DateTimeType),
        th.Property("holdUntilDate", th.DateTimeType),
        th.Property(
            "weight",
            th.ObjectType(
                th.Property("value", th.NumberType), th.Property("units", th.StringType)
            ),
        ),
        th.Property(
            "dimensions",
            th.ObjectType(
                th.Property("units", th.StringType),
                th.Property("length", th.NumberType),
                th.Property("width", th.NumberType),
                th.Property("height", th.NumberType),
            ),
        ),
        th.Property(
            "insuranceOptions",
            th.ObjectType(
                th.Property("provider", th.StringType),
                th.Property("insureShipment", th.BooleanType),
                th.Property("insuredValue", th.NumberType),
            ),
        ),
        th.Property(
            "internationalOptions",
            th.ObjectType(
                th.Property("contents", th.StringType),
                th.Property(
                    "customsItems",
                    th.ArrayType(
                        th.ObjectType(
                            th.Property("customsItemId", th.IntegerType),
                            th.Property("description", th.StringType),
                            th.Property("quantity", th.IntegerType),
                            th.Property("value", th.NumberType),
                            th.Property("harmonizedTariffCode", th.StringType),
                            th.Property("countryOfOrigin", th.StringType),
                        )
                    ),
                ),
                th.Property("nonDelivery", th.StringType),
            ),
        ),
        th.Property(
            "advancedOptions",
            th.ObjectType(
                th.Property("warehouseId", th.IntegerType),
                th.Property("nonMachinable", th.BooleanType),
                th.Property("saturdayDelivery", th.BooleanType),
                th.Property("containsAlcohol", th.BooleanType),
                th.Property("mergedOrSplit", th.BooleanType),
                th.Property("mergedIds", th.ArrayType(th.IntegerType)),
                th.Property("parentId", th.IntegerType),
                th.Property("storeId", th.IntegerType),
                th.Property("customField1", th.StringType),
                th.Property("customField2", th.StringType),
                th.Property("customField3", th.StringType),
                th.Property("source", th.StringType),
                th.Property("billToAccount", th.StringType),
                th.Property("billToPostalCode", th.StringType),
                th.Property("billToCountryCode", th.StringType),
            ),
        ),
        # th.Property("tagIds", th.ArrayType),
        th.Property("userId", th.StringType),
        th.Property("externallyFulfilled", th.BooleanType),
        th.Property("externallyFulfilledBy", th.StringType),
        th.Property("total", th.IntegerType),
        th.Property("page", th.IntegerType),
        th.Property("pages", th.IntegerType),
    ).to_dict()


class ProductsStream(shipstationStream):
    """Define custom stream."""

    name = "products"
    path = "/products"
    records_jsonpath = "$.products[*]"
    primary_keys = ["productId"]
    # replication_key = "modifyDate"

    schema = th.PropertiesList(
        th.Property("productId", th.IntegerType),
        th.Property("aliases", th.StringType),
        th.Property("productId", th.IntegerType),
        th.Property("sku", th.StringType),
        th.Property("name", th.StringType),
        th.Property("price", th.NumberType),
        th.Property("defaultCost", th.NumberType),
        th.Property("length", th.NumberType),
        th.Property("width", th.NumberType),
        th.Property("height", th.NumberType),
        th.Property("weightOz", th.NumberType),
        th.Property("internalNotes", th.StringType),
        th.Property("fulfillmentSku", th.StringType),
        th.Property("createDate", th.StringType),
        th.Property("modifyDate", th.StringType),
        th.Property("active", th.BooleanType),
        th.Property(
            "productCategory",
            th.ObjectType(
                th.Property("categoryId", th.IntegerType),
                th.Property("name", th.StringType),
            ),
        ),
        th.Property(
            "productType",
            th.ObjectType(
                th.Property("customCountryCode", th.StringType),
                th.Property("customDescription", th.StringType),
                th.Property("customTariffNo", th.StringType),
                th.Property("customValue", th.StringType),
                th.Property("defaultCarrierCode", th.StringType),
                th.Property("defaultIntCarrierCode", th.StringType),
                th.Property("defaultIntPackageCode", th.StringType),
                th.Property("defaultIntServiceCode", th.StringType),
                th.Property("defaultIntServiceCode", th.StringType),
                th.Property("defaultPackageCode", th.StringType),
                th.Property("defaultServiceCode", th.StringType),
                th.Property("height", th.NumberType),
                th.Property("lenght", th.NumberType),
                th.Property("name", th.StringType),
                th.Property("noCustoms", th.CustomType({"type": ["boolean", "string"]})),
                th.Property("ProductTypeId", th.IntegerType),
                th.Property("weightOz", th.NumberType),
                th.Property("width", th.NumberType),
            ),
        ),
        th.Property("warehouseLocation", th.StringType),
        th.Property("defaultCarrierCode", th.StringType),
        th.Property("defaultServiceCode", th.StringType),
        th.Property("defaultPackageCode", th.StringType),
        th.Property("defaultIntlCarrierCode", th.StringType),
        th.Property("defaultIntlServiceCode", th.StringType),
        th.Property("defaultIntlPackageCode", th.StringType),
        th.Property("defaultConfirmation", th.StringType),
        th.Property("defaultIntlConfirmation", th.StringType),
        th.Property("customsDescription", th.StringType),
        th.Property("customsValue", th.NumberType),
        th.Property("customsTariffNo", th.StringType),
        th.Property("customsCountryCode", th.StringType),
        th.Property("noCustoms", th.CustomType({"type": ["boolean", "string"]})),
        th.Property(
            "tags",
            th.ArrayType(
                th.ObjectType(
                    th.Property("tagId", th.IntegerType),
                    th.Property("name", th.StringType),
                )
            ),
        ),
        th.Property("total", th.IntegerType),
        th.Property("page", th.IntegerType),
        th.Property("pages", th.IntegerType),
    ).to_dict()
